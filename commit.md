# Useful commit commands

## Show commits

``` git log ```

## Join info to a existing commit

``` git commit --amend -m "add some info" ```

## Remove file from stage area

``` git reset HEAD file.txt ```
