# Workdir commands

## Show changes

In work dir

``` git diff ```

On stage area

``` git diff --staged ```

## Remove file

``` git rm file ```

## Merge

``` git merge ```

### no fastfoward

The --no-ff flag causes the merge to aways create a new commit object, even if the merge could be perfomed with a fast-foward. This avoid losing information about the historical existence of a feature brnach and groups together all commits that together added the feature.
``` git merge --no-ff ```

![plot](./images/no_fast_foward.png)
