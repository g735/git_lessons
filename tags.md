# Tags

## Create tag

### Current commit

``` git tag -a v1.0.0 -m "new release" ```

### Specific commit

``` git tag -a v0.5.0 729b09974b7b458461561b28b647e7444486095d  -m "Release Candidate " ```

## Show tags

### All tags

``` git tag ```  

### Specific tag

``` git show v0.5.0 ```

## Delete tag

``` git tag -d v0.5.0 ```
