
### Initial config
```
git config --global user.name "User Name"
git config --global user.mail "user.name@email.com"
```

### Start a new repository on an existing folder
```
cd folder
git init
git remote add  origin https://gitlab.com/g735/git_lessons.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

### Clone a remote repository
```
git clone https://gitlab.com/g735/git_lessons.git
cd git_lessons
touch file.txt
git add file.txt
git commit -m "add file"
git push -u origin main
```

### Create a switch to a new branch
```
git checkout -b develop
```